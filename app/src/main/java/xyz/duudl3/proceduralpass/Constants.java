package xyz.duudl3.proceduralpass;

public class Constants {
    public static final String APP_NAME = "Procedural Pass";
    public static final String APP_VERSION = "v1.4.3";
}
