package xyz.duudl3.proceduralpass.main;

import android.content.Context;

import java.io.File;
import java.io.FileOutputStream;

public class MainActivityPresenter implements MainActivityContract.MainActivityPresenter {

    private MainActivityContract.MainActivityView view;
    private File walletDir; //Context.getCacheDir();
    public static MainActivityPresenter INSTANCE;

    public MainActivityPresenter(MainActivityContract.MainActivityView view, File walletDir) {
        this.view = view;
        this.walletDir = walletDir;
        INSTANCE = this;
        view.setPresenter(this);
    }

    @Override
    public void subscribe() {

    }

    @Override
    public void unsubscribe() {

    }

    @Override
    public void openAddLogin()
    {
        view.displayMasterpassPrompt(false);
        view.displayAddLoginWindow(true);
    }

    @Override
    public void closeAddLogin()
    {
        view.displayMasterpassPrompt(false);
        view.displayAddLoginWindow(false);
    }
}
