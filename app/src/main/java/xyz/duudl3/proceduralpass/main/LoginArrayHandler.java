package xyz.duudl3.proceduralpass.main;

import android.media.MediaScannerConnection;
import android.os.Environment;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.ArrayList;

public class LoginArrayHandler {
    public static LoginArrayHandler INSTANCE;
    public ArrayList<LoginEntry> logins;

    public LoginArrayHandler()
    {
        INSTANCE = this;
        loadLogins();
        saveLogins();
    }

    public void saveLogins()
    {
        String directory = Environment.DIRECTORY_DOCUMENTS;
        File dir = Environment.getExternalStoragePublicDirectory(directory + "/proceduralpass");

        if(!dir.exists())   {
            dir.mkdirs();
            dir.setWritable(true, true);
            dir.setReadable(true, true);
        }
        File newfile = new File(dir, "data.txt");
        newfile.setWritable(true, true);
        newfile.setReadable(true, true);

        try
        {
            BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(newfile), "UTF-8"));

            for(LoginEntry login : logins)
            {
                writer.write(login.getWebsite() + "=" + login.getUsername() + "=" + login.getUnixTime());
                writer.newLine();
            }

            writer.flush();
            writer.close();
            writer = null;

        }
        catch (IOException e)
        {
            e.printStackTrace();
        }

    }

    public void loadLogins()
    {
        String directory = Environment.DIRECTORY_DOCUMENTS;
        File dir = Environment.getExternalStoragePublicDirectory(directory + "/proceduralpass");

        if(!dir.exists())   {
            dir.mkdirs();
            dir.setWritable(true, true);
            dir.setReadable(true, true);
        }
        File f = new File(dir, "data.txt");
        f.setWritable(true, true);
        f.setReadable(true, true);
        if(f.exists() && !f.isDirectory()) {
            logins = new ArrayList<LoginEntry>();
            try (BufferedReader br = new BufferedReader(new FileReader(f.getPath()))) {
                String line;
                while ((line = br.readLine()) != null) {
                    String[] combo = line.split("=");
                    String website = combo[0];
                    String username = combo[1];
                    String unixTime = "";

                    if(combo.length == 3)
                    {
                        unixTime = combo[2];
                    }

                    //after v1.3.0 this will be phased out slowly.
                    if (combo.length == 4)
                    {
                        unixTime = combo[2];
                    }

                    logins.add(new LoginEntry(username, website, unixTime));
                    System.out.println("ADDED LOGIN: " + username + " " + website + " " + unixTime);
                }
            } catch (FileNotFoundException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        else
        {
            logins = new ArrayList<LoginEntry>();
        }
    }
}
