package xyz.duudl3.proceduralpass.main;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.media.MediaScannerConnection;
import android.os.Environment;
import android.os.Looper;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.text.InputType;
import android.text.method.LinkMovementMethod;
import android.view.ContextThemeWrapper;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.androidannotations.annotations.AfterInject;
import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.OptionsItem;
import org.androidannotations.annotations.OptionsMenu;
import org.androidannotations.annotations.SystemService;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;
import org.androidannotations.annotations.res.ColorRes;
import org.androidannotations.annotations.res.StringRes;
import xyz.duudl3.proceduralpass.Constants;
import xyz.duudl3.proceduralpass.R;
import xyz.duudl3.proceduralpass.ui.NonScrollListView;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

@EActivity(R.layout.activity_main)
@OptionsMenu(R.menu.menu_main)
public class MainActivity extends AppCompatActivity implements MainActivityContract.MainActivityView {

    private MainActivityContract.MainActivityPresenter presenter;

    @ViewById
    protected FrameLayout confirmWindow;
    @ViewById
    protected TextView confirmTitle;
    @ViewById
    protected Button btnConfirm;
    @ViewById
    protected Button btnDeny;

    @ViewById
    protected FrameLayout addLoginWindow;
    @ViewById
    protected EditText editUser;
    @ViewById
    protected EditText editSite;
    @ViewById
    protected Button btnAddLoginEntry;
    @ViewById
    protected Button btnCloseAddLogin;

    @ViewById
    protected FrameLayout masterpassWindow;
    @ViewById
    protected EditText editMasterpass;
    @ViewById
    protected Button openButton;

    @ViewById
    protected Toolbar toolbar_AT;
    @ViewById
    protected Button btnAddLogin;

    @ViewById
    protected NonScrollListView lv_logins;

    public static String availChars = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ!@#$%^&*()";
    public static String masterPassword;

    @SystemService
    protected ClipboardManager clipboardManager;

    @StringRes(R.string.about)
    protected String strAbout;

    @ColorRes(android.R.color.holo_green_dark)
    protected int colorGreenDark;
    @ColorRes(android.R.color.darker_gray)
    protected int colorGreyDark;

    SharedPreferences prefs = null;
    public static boolean isNewUser = true;

    @AfterInject
    protected void initData() {
        LoginArrayHandler loginHandler = new LoginArrayHandler();
        new MainActivityPresenter(this, getCacheDir());
    }

    @SuppressLint("ApplySharedPref")
    @AfterViews
    protected void initUI() {
        initToolbar();
        setListeners();
        addLoginWindow.setVisibility(View.GONE);
        confirmWindow.setVisibility(View.GONE);
        prefs = getSharedPreferences("xyz.duudl3.proceduralpass", MODE_PRIVATE);

        if (prefs.getBoolean("isNewUser", true)) {
            // Do first run stuff here then set 'firstrun' as false
            // using the following line to edit/commit prefs
            isNewUser = true;
            prefs.edit().putBoolean("isNewUser", false).commit();
        }
        else
        {
            isNewUser = false;
        }

        presenter.subscribe();

        if(isNewUser)
            displayBackupDialog();

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED)
        {
            if (!ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.WRITE_EXTERNAL_STORAGE))
            {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 5);
            }
        }

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED)
        {
            if (!ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_EXTERNAL_STORAGE))
            {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 6);
            }
        }
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_HOME);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    public void displayBackupDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Notice");
        builder.setMessage("Due to the procedural password generation algorithm being ported from a desktop version, copying passwords in this mobile " +
                "version will be relatively slow.");
        builder.setCancelable(true);
        builder.setPositiveButton("Got it", (dialog, which) -> dialog.dismiss());
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
        alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(Color.BLACK);
        TextView msgTxt = (TextView) alertDialog.findViewById(android.R.id.message);
        msgTxt.setMovementMethod(LinkMovementMethod.getInstance());
    }

    private void initToolbar() {
        setSupportActionBar(toolbar_AT);
        if(getSupportActionBar() != null) {
            getSupportActionBar().setTitle(Constants.APP_NAME + " " + Constants.APP_VERSION);
        }
    }

    @Override
    public void setPresenter(MainActivityContract.MainActivityPresenter presenter) {
        this.presenter = presenter;
    }

    @Override
    @UiThread
    public void displayMasterpassPrompt(boolean isShown) {
        masterpassWindow.setVisibility(isShown ? View.VISIBLE : View.GONE);
    }

    @Override
    @UiThread
    public void displayAddLoginWindow(boolean isShown) {
        addLoginWindow.setVisibility(isShown ? View.VISIBLE : View.GONE);
        editUser.setText(null);
        editSite.setText(null);
    }

    @Override
    public void displayConfirmWindow(String operation, int position) {
        confirmWindow.setVisibility(View.VISIBLE);

        if(operation == "reset")
            confirmTitle.setText("Are you sure you wish to reset?");
        else if(operation == "delete")
            confirmTitle.setText("Are you sure you wish to delete?");

        btnConfirm.setOnClickListener(v -> performOperation(operation, position));
        btnDeny.setOnClickListener(v -> closeConfirmWindow());
    }

    public void performOperation(String operation, int position)
    {
        LoginEntry entry = LoginArrayHandler.INSTANCE.logins.get(position);

        if(operation == "reset")
        {
            long unixTimestamp = System.currentTimeMillis() / 1000L;
            LoginArrayHandler.INSTANCE.logins.set(position, new LoginEntry(entry.getUsername(), entry.getWebsite(), unixTimestamp + ""));
            LoginArrayHandler.INSTANCE.saveLogins();
            setArrayAdapter();
        }
        else if(operation == "delete")
        {
            LoginArrayHandler.INSTANCE.logins.remove(entry);
            LoginArrayHandler.INSTANCE.saveLogins();
            setArrayAdapter();
        }

        confirmWindow.setVisibility(View.GONE);
    }

    public void closeConfirmWindow()
    {
        confirmWindow.setVisibility(View.GONE);
        editUser.setText(null);
        editSite.setText(null);
    }

    @Override
    public void displayLogin(LoginEntry entry, int position) {
        AlertDialog.Builder builder = new AlertDialog.Builder(new ContextThemeWrapper(this, R.style.AlertDialogCustom));
        builder.setTitle(Html.fromHtml("<b>Login Actions</b>"));
        builder.setItems(new CharSequence[]
                        {"Get Pass", "Reset", "Delete", "Close"},
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // The 'which' argument contains the index position
                        // of the selected item
                        switch (which) {
                            case 0:
                                (new Thread(){
                                    @Override
                                    public void run()
                                    {
                                        Looper.prepare();
                                        MainActivity.this.runOnUiThread(new Runnable() {
                                            public void run() {
                                                Toast.makeText(MainActivity.this, "Generating password...", Toast.LENGTH_SHORT).show();
                                            }
                                        });
                                        ClipData clip = null;
                                        try {
                                            String password = entry.generatePassword(entry.getUsername(), entry.getWebsite(), entry.getUnixTime(), masterPassword);
                                            clip = ClipData.newPlainText("Login", password);
                                        } catch (UnsupportedEncodingException e) {
                                            e.printStackTrace();
                                        }
                                        clipboardManager.setPrimaryClip(clip);
                                        MainActivity.this.runOnUiThread(new Runnable() {
                                            public void run() {
                                                Toast.makeText(MainActivity.this, "Generated password!", Toast.LENGTH_SHORT).show();
                                            }
                                        });
                                    }
                                }).start();
                                break;
                            case 1:
                                displayConfirmWindow("reset", position);
                                break;
                            case 2:
                                displayConfirmWindow("delete", position);
                                break;
                            case 3:
                                dialog.dismiss();
                                break;
                        }
                    }
                });
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    private void setListeners() {
        openButton.setOnClickListener(v -> open());
        btnAddLogin.setOnClickListener(v -> presenter.openAddLogin());
        btnAddLoginEntry.setOnClickListener(v -> addLoginEntry());
        btnCloseAddLogin.setOnClickListener(v -> presenter.closeAddLogin());
        lv_logins.setOnItemClickListener((parent, view, position, id) -> {
            LoginEntry entry = LoginArrayHandler.INSTANCE.logins.get(position);
            displayLogin(entry, position);
        });
    }

    @Override
    public void addLoginEntry()
    {
        if(editUser.getText().toString().length() != 0 && editSite.getText().toString().length() != 0 && !editUser.getText().toString().contains("=") && !editSite.getText().toString().contains("=")) {
            long unixTimestamp = System.currentTimeMillis() / 1000L;
            LoginArrayHandler.INSTANCE.logins.add(new LoginEntry(editUser.getText().toString(), editSite.getText().toString(), unixTimestamp + ""));
            LoginArrayHandler.INSTANCE.saveLogins();
            setArrayAdapter();
            displayAddLoginWindow(false);
        }
    }

    @Override
    public void open()
    {
        if(editMasterpass.getText().length() != 0) {
            masterPassword = editMasterpass.getText().toString();
            masterPassword = LoginEntry.SHA256(masterPassword + "&GH&*BT6VR6r6v6&B&H7H7h&H77ybn&y7TBV");
            displayMasterpassPrompt(false);
            displayAddLoginWindow(false);
            editMasterpass.setText("000000000000");
            setArrayAdapter();
        }
    }

    @Override
    public void setArrayAdapter()
    {
        setListViewShit();
    }

    public void setListViewShit()
    {
        ArrayList<String> loginsFormatted = new ArrayList<String>();
        for(LoginEntry entry : LoginArrayHandler.INSTANCE.logins)
        {
            loginsFormatted.add(entry.getUsername() + " | " + entry.getWebsite());
        }

        ArrayAdapter<String> itemsAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, loginsFormatted){
            @Override
            public View getView(int position, View convertView, ViewGroup parent){
                // Get the Item from ListView
                View view = super.getView(position, convertView, parent);

                // Initialize a TextView for ListView each Item
                TextView tv = (TextView) view.findViewById(android.R.id.text1);

                // Set the text color of TextView (ListView Item)
                tv.setTextColor(Color.WHITE);

                // Generate ListView Item using TextView
                return view;
            }
        };
        lv_logins.setAdapter(itemsAdapter);

        int desiredWidth = View.MeasureSpec.makeMeasureSpec(lv_logins.getWidth(), View.MeasureSpec.UNSPECIFIED);
        int totalHeight = 0;
        View view = null;

        for (int i = 0; i < itemsAdapter.getCount(); i++) {
            view = itemsAdapter.getView(i, view, lv_logins);

            if (i == 0)
                view.setLayoutParams(new ViewGroup.LayoutParams(desiredWidth, ViewGroup.LayoutParams.WRAP_CONTENT));

            view.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);
            totalHeight += view.getMeasuredHeight();
        }

        ViewGroup.LayoutParams params = lv_logins.getLayoutParams();
        params.height = totalHeight + (lv_logins.getDividerHeight() * (itemsAdapter.getCount() - 1));

        lv_logins.setLayoutParams(params);
        lv_logins.requestLayout();
    }
}