package xyz.duudl3.proceduralpass.main;

import xyz.duudl3.proceduralpass.base.BasePresenter;
import xyz.duudl3.proceduralpass.base.BaseView;

public interface MainActivityContract {
    interface MainActivityView extends BaseView<MainActivityPresenter> {
        void displayMasterpassPrompt(boolean isShown);
        void displayAddLoginWindow(boolean isShown);
        void displayConfirmWindow(String operation, int position);
        void displayLogin(LoginEntry entry, int position);
        void setArrayAdapter();
        void addLoginEntry();
        void open();
    }
    interface MainActivityPresenter extends BasePresenter {
        void openAddLogin();
        void closeAddLogin();
    }
}
