package xyz.duudl3.proceduralpass.base;

public interface BaseView<T extends BasePresenter> {
    void setPresenter(T presenter);
}
