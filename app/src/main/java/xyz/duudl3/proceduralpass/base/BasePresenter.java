package xyz.duudl3.proceduralpass.base;

public interface BasePresenter {
    void subscribe();
    void unsubscribe();
}
